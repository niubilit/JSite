/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.service;

import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.flowable.dao.FormDataDao;
import com.jsite.modules.flowable.entity.FormData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 表单数据表生成Service
 * @author liuruijun
 * @version 2019-04-01
 */
@Service
@Transactional(readOnly = true)
public class FormDataService extends CrudService<FormDataDao, FormData> {

	@Override
	public FormData get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<FormData> findList(FormData formData) {
		return super.findList(formData);
	}

	public List<FormData> findTempList(FormData formData) {
		return dao.findTempList(formData);
	}

	@Override
	public Page<FormData> findPage(Page<FormData> page, FormData formData) {
		return super.findPage(page, formData);
	}

	@Override
	@Transactional(readOnly = false)
	public void save(FormData formData) {
		super.save(formData);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(FormData formData) {
		super.delete(formData);
	}


    @Transactional(readOnly = false)
    public void saveTemp(FormData entity) {
        FormData temp = dao.getTemp(entity);
        if (temp == null){
            entity.preInsert();
            dao.insertTemp(entity);
        }else{
            entity.preUpdate();
            dao.updateTemp(entity);
        }
    }
    @Transactional(readOnly = false)
    public void deleteTemp(FormData formData) {
	    dao.deleteTemp(formData);
    }

	@Transactional(readOnly = false)
	public void insert(FormData formData) {
		dao.insert(formData);
	}
}