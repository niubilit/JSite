/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;
import com.jsite.common.persistence.DataEntity;
import com.jsite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

/**
 * 伊利员工社保信息表Entity
 * @author liuruijun
 * @version 2020-07-29
 */
public class YiliEmployee extends DataEntity<YiliEmployee> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 员工编号
	private String name;		// 员工姓名
	private String gender;		// 性别
	private Date birthday;		// 出生日期
	private String age;		// 年龄
	private String mobile;		// 手机号
	private Office office;		// 所属部门ID
	private List<YiliSocialSecurityInfo> yiliSocialSecurityInfoList = Lists.newArrayList();		// 子表列表
	
	public YiliEmployee() {
		super();
	}

	public YiliEmployee(String id){
		super(id);
	}

	@Length(min=1, max=64, message="员工编号长度必须介于 1 和 64 之间")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Length(min=0, max=64, message="员工姓名长度必须介于 0 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=1, message="性别长度必须介于 0 和 1 之间")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	@Length(min=0, max=11, message="年龄长度必须介于 0 和 11 之间")
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
	@Length(min=0, max=32, message="手机号长度必须介于 0 和 32 之间")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}
	
	public List<YiliSocialSecurityInfo> getYiliSocialSecurityInfoList() {
		return yiliSocialSecurityInfoList;
	}

	public void setYiliSocialSecurityInfoList(List<YiliSocialSecurityInfo> yiliSocialSecurityInfoList) {
		this.yiliSocialSecurityInfoList = yiliSocialSecurityInfoList;
	}
}