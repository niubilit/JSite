/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.web;

import com.jsite.common.config.Global;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.web.BaseController;
import com.jsite.modules.yili.entity.YiliEmployee;
import com.jsite.modules.yili.entity.YiliSocialSecurityInfo;
import com.jsite.modules.yili.service.YiliEmployeeService;
import com.jsite.modules.yili.service.YiliSocialSecurityInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * 社保信息表Controller
 * @author liuruijun
 * @version 2020-07-29
 */
@Controller
@RequestMapping(value = "${adminPath}/yili/yiliSocialSecurityInfo")
public class YiliSocialSecurityInfoController extends BaseController {

	@Autowired
	private YiliSocialSecurityInfoService yiliSocialSecurityInfoService;

	@Autowired
	private YiliEmployeeService yiliEmployeeService;
	@ModelAttribute
	public YiliSocialSecurityInfo get(@RequestParam(required=false) String id) {
		YiliSocialSecurityInfo entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = yiliSocialSecurityInfoService.get(id);
		}
		if (entity == null){
			entity = new YiliSocialSecurityInfo();
		}
		return entity;
	}
	
	@RequiresPermissions("yili:yiliSocialSecurityInfo:view")
	@RequestMapping(value = {"list", ""})
	public String list(YiliSocialSecurityInfo yiliSocialSecurityInfo, Model model) {
		model.addAttribute("yiliSocialSecurityInfo", yiliSocialSecurityInfo);
		YiliEmployee yiliEmployee = yiliEmployeeService.get(yiliSocialSecurityInfo.getEmployeeId());
		model.addAttribute("yiliEmployee", yiliEmployee);
		return "modules/yili/yiliSocialSecurityInfoList";
	}
	
	@RequiresPermissions("yili:yiliSocialSecurityInfo:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<YiliSocialSecurityInfo> listData(YiliSocialSecurityInfo yiliSocialSecurityInfo, HttpServletRequest request, HttpServletResponse response) {
		Page<YiliSocialSecurityInfo> page = yiliSocialSecurityInfoService.findPage(new Page<>(request, response), yiliSocialSecurityInfo);
		return page;
	}

	@RequiresPermissions("yili:yiliSocialSecurityInfo:view")
	@RequestMapping(value = "form")
	public String form(YiliSocialSecurityInfo yiliSocialSecurityInfo, Model model) {
		YiliEmployee employee = yiliEmployeeService.get(yiliSocialSecurityInfo.getEmployeeId());
		yiliSocialSecurityInfo.setEmployeeName(employee.getName());
		model.addAttribute("yiliSocialSecurityInfo", yiliSocialSecurityInfo);
		return "modules/yili/yiliSocialSecurityInfoForm";
	}

	@RequiresPermissions("yili:yiliSocialSecurityInfo:edit")
	@RequestMapping(value = "save")
	@ResponseBody
	public String save(YiliSocialSecurityInfo yiliSocialSecurityInfo) {
		if (yiliSocialSecurityInfo.getId().equals("-1")) {
			yiliSocialSecurityInfo.setId("");
		}
		if (yiliSocialSecurityInfo.getIsNewRecord()) {
			// TODO 获取同月份同类型的缴费信息，如果存在则返回失败
//			yiliSocialSecurityInfoService.getByTypeAndMonth(yiliSocialSecurityInfo);
//			return false;
		}

		BigDecimal b = new BigDecimal(yiliSocialSecurityInfo.getPaymentAmount());
		yiliSocialSecurityInfo.setPaymentAmount(b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

		yiliSocialSecurityInfoService.save(yiliSocialSecurityInfo);
		return renderResult(Global.TRUE, "保存社保信息成功");
	}
	
	@RequiresPermissions("yili:yiliSocialSecurityInfo:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(YiliSocialSecurityInfo yiliSocialSecurityInfo) {
		yiliSocialSecurityInfoService.delete(yiliSocialSecurityInfo);
		return renderResult(Global.TRUE, "删除社保信息成功");
	}

}